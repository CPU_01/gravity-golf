﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitParent : MonoBehaviour {

    public float rotationSpeed;

    Transform parentTransform;

	// Use this for initialization
	void Start () {
        if (transform.parent)
        {
            parentTransform = transform.parent;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (parentTransform == null)
            return;

        transform.RotateAround(parentTransform.position, Vector3.forward, rotationSpeed * Time.deltaTime);
	}
}
