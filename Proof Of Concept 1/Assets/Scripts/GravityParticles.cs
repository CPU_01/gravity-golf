﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityParticles : MonoBehaviour {

	private float parentMass;
	public float gravitationalConstant;
	public float outermostGravityLimit;

	// Make particle system move outside -> in
	// Make them accelerate according to gravity, same as the sattelite will experience
	// Change color based on speed
	// Or based on lifetime, and have them be flat disks that also change size over lifetime?

	// Use this for initialization
	void Start () {
		parentMass = transform.parent.GetComponent<Rigidbody2D> ().mass;
		var gravityParticles = GetComponent<ParticleSystem> ().main;

		// startSize isn't a radius, its a diameter
		float initialParticleSize = Mathf.Sqrt (gravitationalConstant*parentMass/outermostGravityLimit)*2f;

		gravityParticles.startSize = initialParticleSize;
		gravityParticles.loop = true;
	}
	
	// Update is called once per frame
	void Update () {
//		print (gravitationalConstant);

	}
}
