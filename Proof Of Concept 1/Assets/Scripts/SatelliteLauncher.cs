﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteLauncher : MonoBehaviour
{

	private bool moving = false;
	private Vector3 launchVector = Vector3.zero;
	private Vector3 velocity = Vector3.zero;
	public float damping = 1f;
    private Rigidbody2D myRB;

    //set to true when the ship is launched
    private bool launched;

    void Awake() {
        myRB = GetComponent<Rigidbody2D>();
        myRB.isKinematic = true;
    }

	// Update is called once per frame
	void Update ()
	{
		if (damping <= 0f) {
			damping = 1f;
		}

	}

	private void Click ()
	{
		if (!moving)
			StartCoroutine (Launch ());

	}

	IEnumerator PullBack ()
	{

		while (Input.GetMouseButton (0)) {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePosition.z = 0f;
			launchVector = transform.position - mousePosition;
			yield return null;
		}

	}

	IEnumerator Launch ()
	{
		moving = true;
		yield return StartCoroutine (PullBack ());
		myRB.isKinematic = false;

        Vector2 newVector = new Vector2(launchVector.x, launchVector.y);
		myRB.velocity = newVector;
		yield return StartCoroutine (Move ());
        launched = true;
		moving = false;
	}

	IEnumerator Move ()
	{
		while (velocity != Vector3.zero) {
			transform.position += velocity / damping;
			yield return null;
		}
	}

    public bool GetLaunched()
    {
        return launched;
    }

    public void SetLaunched(bool boolean)
    {
        launched = boolean;
    }
}
