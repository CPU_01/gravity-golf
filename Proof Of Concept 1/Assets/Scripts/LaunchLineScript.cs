﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchLineScript : MonoBehaviour {

    LineRenderer launchLine;
    Vector3 mousePos;
    bool isMouseDown;

    // Use this for initialization
    void Awake ()
    {
        launchLine = gameObject.GetComponent<LineRenderer>();
        launchLine.SetPosition(0, transform.position);
        launchLine.SetPosition(1, transform.position);
        launchLine.enabled = false;
        isMouseDown = false;
	}
	
	// Update is called once per frame
	void Update () {
        launchLine.SetPosition(0, transform.position);
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        
        if(isMouseDown)
        {
            launchLine.SetPosition(1, (-mousePos) + (transform.position*2));
        }

    }

    private void OnMouseDown()
    {
        isMouseDown = true;
        launchLine.enabled = true;
    }

    private void OnMouseUp()
    {
        launchLine.enabled = false;
    }
}
