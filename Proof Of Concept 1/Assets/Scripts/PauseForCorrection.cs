﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseForCorrection : MonoBehaviour {

    ScoreScript scoreScript;
    SatelliteLauncher shipScript;
    float timer;

    public Text pauseText;
    public Text timerText;

	// Use this for initialization
	void Start () {
        shipScript = GameObject.FindGameObjectWithTag("Player").GetComponent<SatelliteLauncher>();
        scoreScript = gameObject.GetComponent<ScoreScript>();
        pauseText = GameObject.FindGameObjectWithTag("PauseText").GetComponent<Text>();
        timerText = GameObject.FindGameObjectWithTag("TimeText").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        //if the ship has been lauched and the player left mouse clicks, tells the game to pause
        if (shipScript.GetLaunched() == true)
        {
            // add time to the timer
            timer += Time.deltaTime;
            if (Input.GetMouseButtonDown(0))
            {
                //pauses the game
                Time.timeScale = 0;
                //tells the manager to make the launched boolean false
                shipScript.SetLaunched(false);
                scoreScript.CorrectionUsed();
                pauseText.text = "Game is paused. Launch Ship to resume." + "\n Press r to restart level, Backspace to reset game, and Esc to exit";
            }
            else
            {
                //resumes the game
                Time.timeScale = 1;
                pauseText.text = "";
            }
        }

        // update the timer text
        UpdateTimerText();

        // timerText.text = Mathf.Floor(timer).ToString();
	}

    void UpdateTimerText()
    {
        float deciseconds = Mathf.Floor(timer * 100) % 100;
        float seconds = Mathf.Floor(timer % 60);
        float minutes = Mathf.Floor((timer / 60) % 60);

        timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + deciseconds.ToString("00");
    }

    public float SetTimeComplete()
    {
        return timer;
    }

}
