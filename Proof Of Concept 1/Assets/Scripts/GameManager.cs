﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool winCondition;

    public Text winText;

    ScoreScript scoreScript;

    //public GameObject spawnerPrefab;

    private Scene currentScene;
    private int currentSceneIndex;



    // Use this for initialization
    void Awake ()
    {
        scoreScript = gameObject.GetComponent<ScoreScript>();
        winText = GameObject.FindGameObjectWithTag("WinText").GetComponent<Text>();
        winCondition = false;

        currentScene = SceneManager.GetActiveScene();
        currentSceneIndex = currentScene.buildIndex;
    }

    private void Start()
    {
        winText.text = "";
    }

    public void LevelWin()
    {
        winCondition = true;
        scoreScript.CalculateScore();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Click();
        }

        if (winCondition)
        {
            winText.text = "Level Complete!";
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(currentSceneIndex, LoadSceneMode.Single);
        }

        if(Input.GetKeyDown(KeyCode.Backspace))
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    
    private void Click()
    {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

		if (hit)
        {
            if (hit.transform.CompareTag("Player"))
            {
                hit.transform.gameObject.SendMessage("Click", SendMessageOptions.DontRequireReceiver);
            }
        }
        else
        {
            //SpawnBlackHole();
        }
    }
    /*
    private void SpawnBlackHole()
    {
        print("blackhole here!");
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0f;
        //GameObject newHole = Instantiate(spawnerPrefab, mousePosition, Quaternion.identity);

        // Include logic to adjust gravity pull here
    }

    */
}
