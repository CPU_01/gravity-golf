﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

    [Tooltip("In seconds")] public float timeToWait;

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, timeToWait);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
