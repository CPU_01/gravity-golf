﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour {

    ScoreScript scoreScript;

	// Use this for initialization
	void Start () {
        scoreScript = GameObject.FindGameObjectWithTag("Manager").GetComponent<ScoreScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            scoreScript.AddPoint();
            Destroy(gameObject);
        }
    }
}
