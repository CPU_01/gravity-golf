﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour {

    public GameObject gameManager;
    GameManager managerScript;
	// Use this for initialization
	void Start ()
    {
        managerScript = gameManager.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {

            managerScript.LevelWin();
            col.gameObject.SetActive(false);
        }
    }
}
