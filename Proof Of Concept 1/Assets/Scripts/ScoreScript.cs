﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

    public Text correctionsText;
    public Text scoreText;

    float correctionsUsed;
    public float score;

    PauseForCorrection timeScript;
    float timeComplete;

    // Use this for initialization
    void Awake () {
        correctionsUsed = 0;
        scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
        correctionsText = GameObject.FindGameObjectWithTag("CorrectionsText").GetComponent<Text>();
        timeScript = GetComponent<PauseForCorrection>();
    }

    void Start()
    {
        correctionsText.text = "";
        scoreText.text = "";

        score = 10;
    }

    // Update is called once per frame
    void Update () {
        correctionsText.text = "Corrections Used: " + correctionsUsed;
	}

    public void CorrectionUsed()
    {
        correctionsUsed += 1f;
    }

    public void CalculateScore()
    {
        timeComplete = timeScript.SetTimeComplete();
        if (correctionsUsed >= score)
        {
            scoreText.text = "Score: " + 0;
        }
        else
        {
            scoreText.text = "Score: " + ((score + Mathf.Floor(timeComplete)) - correctionsUsed);
        }
    }

    public void AddPoint()
    {
        score += 1f;
    }
}
