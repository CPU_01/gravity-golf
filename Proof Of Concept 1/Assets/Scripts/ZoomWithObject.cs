﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomWithObject : MonoBehaviour {

    Camera mainCamera;
    public GameObject subject;
    Vector2 subjectPos;

    public float minZoom = 5; 
    public float maxZoom = 10;

    Vector2 center;
    float distance;

	// Use this for initialization
	void Start () {
        mainCamera = GetComponent<Camera>();
        center = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (subject)
        {
            CheckDistance();
            Zoom();
        }   
	}

    void CheckDistance()
    {
        if (subject)
        subjectPos = subject.transform.position;
        Vector2 distanceVector = center - subjectPos;
        distance = distanceVector.magnitude;
    }

    void Zoom()
    {
        if (distance < minZoom)
            distance = minZoom;
        if (distance > maxZoom)
            distance = maxZoom;
        mainCamera.orthographicSize = distance;
    }
}
