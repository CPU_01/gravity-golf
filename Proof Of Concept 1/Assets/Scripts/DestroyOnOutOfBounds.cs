﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnOutOfBounds : MonoBehaviour {

    GameOverOnDestroy gameOverScript;

    private void Awake()
    {
        gameOverScript = GetComponent<GameOverOnDestroy>();
    }

    private void OnBecameInvisible()
    {
        if (gameObject.active == true)
            Destroy(this.gameObject);
    }
}
