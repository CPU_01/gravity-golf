﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverOnDestroy : MonoBehaviour {

    public Text gameOverText;

    public string gameOverString = "Game Over";

    private void Awake()
    {
        gameOverText = GameObject.FindGameObjectWithTag("GameOverText").GetComponent<Text>();
    }

    private void Start()
    {
        
        gameOverText.text = gameOverString;
        gameOverText.enabled = false;
    }

    public void SetGameOverString(string newGameOverString)
    {
        gameOverText.text = newGameOverString;

    }

    private void OnDestroy()
    {
        if (gameOverText)
            gameOverText.enabled = true;
    }
}
