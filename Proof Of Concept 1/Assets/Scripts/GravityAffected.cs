﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityAffected : MonoBehaviour {

    Rigidbody2D rb2D;

    public static List<GameObject> affectors = new List<GameObject>();

    public float gravConstant = 0.667408F;

    public Vector2 initialVelocity;


	// Use this for initialization
	void Start () {

        // add all affectors that are in the scene to the list of affectors.
        GameObject[] startAffectors = GameObject.FindGameObjectsWithTag("Gravity");

        if (startAffectors != null)
        {
            for (int affectorIndex = 0; affectorIndex < startAffectors.Length; ++affectorIndex)
            {
                affectors.Add(startAffectors[affectorIndex]);
            }
        }

        // get the object's rigidbody.
        rb2D = GetComponent<Rigidbody2D>();

        // apply some velocity for testing purposes.
        rb2D.AddForce(initialVelocity, ForceMode2D.Impulse);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        ApplyGravity();

        ApplyControl();
	}

    void ApplyGravity()
    {
        // for each affector in the list...
        for (int affectorIndex = 0; affectorIndex < affectors.Count; ++affectorIndex)
        {
            if (affectors[affectorIndex] != this.gameObject)
            {
                // grab rigidbody
                Rigidbody2D affectorRB = affectors[affectorIndex].GetComponent<Rigidbody2D>();

                // find the gravitational force. this is F = G(m1 * m2)Dsquared
                float gravStrength = gravConstant * (rb2D.mass * affectorRB.mass) / Mathf.Pow(Vector2.Distance(rb2D.position, affectorRB.position), 2);
                //Debug.Log(gravStrength);
                Vector2 gravForce = (affectorRB.position - rb2D.position).normalized * gravStrength;
                //Debug.Log(gravForce);

                rb2D.AddForce(gravForce);
            }
        }
    }

    void ApplyControl()
    {
        rb2D.AddForce(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));
    }
}
