﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartFunctions : MonoBehaviour {
	private Scene currentScene;
	private int currentSceneIndex;

	private void Awake(){
		currentScene = SceneManager.GetActiveScene ();
		currentSceneIndex = currentScene.buildIndex;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RestartLevel(){
		// Restart the current level from scratch

		SceneManager.LoadScene(currentSceneIndex, LoadSceneMode.Single);
	}

	public void RestartGame(){
		// Reload the very first scene regardless of what it is
		// Either the very first level or the main menu, should one exist

		SceneManager.LoadScene (0, LoadSceneMode.Single);
	}
}
