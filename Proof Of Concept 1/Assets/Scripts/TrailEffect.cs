﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailEffect : MonoBehaviour {

    public GameObject particle;
    public float spawnRate;
    float timer;

    private void Start()
    {
        timer = Time.time + spawnRate;
    }

    // Update is called once per frame
    void Update () {
        if (Time.time >= timer)
        {
            Instantiate(particle, transform.position, Quaternion.identity, transform.parent);
            timer = Time.time + spawnRate;
        }

	}
}
